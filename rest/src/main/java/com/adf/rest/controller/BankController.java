package com.adf.rest.controller;

import java.util.List;
import com.adf.rest.dao.BankDao;
import com.adf.rest.dao.TransactionDao;
import com.adf.rest.models.BankAccount;
import com.adf.rest.models.Transaction;
import com.adf.rest.request.CreateRequest;
import com.adf.rest.validator.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.adf.rest.common.BadRequestException;
import com.adf.rest.common.Error;

@RestController
public class BankController {
    @Autowired
    BankDao dao;
    
    @Autowired
    TransactionDao da;
    
    @Autowired
    private Validator validator;
    
    @PostMapping(path="bank/account",consumes={"application/json"})
    public List<BankAccount> home(@RequestBody CreateRequest obj)
    {
    	Transaction obt=new Transaction();
        System.out.println(obj.getAccountType()+obj.getAccountHolderName()+obj.getDateofBirth()+obj.getInitialDeposit());
        BankAccount objAcc = new BankAccount();
        objAcc.setAccountHolderName(obj.getAccountHolderName());
        objAcc.setAccountType(obj.getAccountType());
        objAcc.setDateofBirth(obj.getDateofBirth());
        
        if ((obj.getInitialDeposit() != 0.0)){
             objAcc.setBalance(obj.getInitialDeposit());
        }
        if ((obj.getAccountType().toLowerCase()).equals("current")){
            objAcc.setTransactionFee(5.0);
        }
        
        List<Error> errors = validator.validateCreateRequest(objAcc);
        // if not success
        
        if(errors.size()>0) {
        	throw new BadRequestException("Bad Request",errors);
        }
        // if success
        
        dao.save(objAcc);
        
        double bal=objAcc.getBalance();
        if ((obj.getAccountType().toLowerCase()).equals("current"))
        {
        	bal=bal-5;
        	obt.setTransactionAmount(bal);
        }
        else
        {
        	obt.setTransactionAmount(bal);
        }

        obt.setTransactionType("Deposit");
        obt.setTransactionStatus("Success");
        obt.setAccount(objAcc);
        da.save(obt);
        
        return dao.findAll();
    }
   
}