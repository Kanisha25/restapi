package com.adf.rest.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.adf.rest.dao.BankDao;
import com.adf.rest.dao.TransactionDao;
import com.adf.rest.models.BankAccount;
import com.adf.rest.models.TransDetails;
import com.adf.rest.request.TransHistoryRequest;

@RestController
public class TransHistoryController {
     
		@Autowired
	    BankDao dao;
		
		 @Autowired
		 TransactionDao da;
	 
	    @GetMapping(path="bank/history/{acc}",consumes={"application/json"})
	    public TransDetails home(@PathVariable("acc") Long AccountNumber,@RequestBody TransHistoryRequest obj)
	    {
	    	
	       System.out.print(obj.getFromDate()+"  hello "+obj.getToDate());
	       
	       TransDetails td = new TransDetails();
	       
	       BankAccount ob = dao.findByAccountNumber(AccountNumber);
	       
	       List<com.adf.rest.models.Transaction> lis = ob.getTransaction();
	       
	       System.out.print(lis);
	       List<com.adf.rest.models.Transaction> lis1 =new ArrayList<>();
	       com.adf.rest.models.Transaction now = null;
	       for(int i=0;i<lis.size();i++) {
	    	   LocalDateTime transdate = lis.get(i).getTransactionDate();
	    	   LocalDateTime fromdate  = obj.getFromDate();
	    	   LocalDateTime todate  = obj.getToDate();
	    	   if((fromdate.compareTo(transdate)== -1) && (todate.compareTo(transdate)== 1)) {
	                 now = lis.get(i);
	    		   
	    	   }
	    	   if(now != null && lis1.contains(now)==false ) {
	    	    lis1.add(now);
	    	   }
	       }
	       
	       System.out.print(lis1);
	       
	       td.setAccountNumber(AccountNumber);
	       td.setAccountHolderName(ob.getAccountHolderName());
	       td.setDateOfBirth(ob.getDateofBirth());
	       td.setBalance(ob.getBalance());
	       td.setAccountType(ob.getAccountType());
	       td.setFromDate(obj.getFromDate());
	       td.setToDate(obj.getToDate());
	       td.setListDetails(lis1);
	       
	       return td;
	    }
	    
	
}
