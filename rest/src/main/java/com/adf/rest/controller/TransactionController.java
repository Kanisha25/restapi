package com.adf.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.adf.rest.dao.BankDao;
import com.adf.rest.dao.TransactionDao;
import com.adf.rest.models.BankAccount;
import com.adf.rest.models.Transaction;
import com.adf.rest.request.TransactionRequest;
@RestController
public class TransactionController {
	 @Autowired
	 TransactionDao da;
	 
	 @Autowired
	 BankDao dao;
	    @PostMapping(path="bank/account/{acc}/transaction",consumes={"application/json"})
	    public Transaction ho(@PathVariable("acc") Long AccountNumber, @RequestBody TransactionRequest obj){
	    	System.out.println(obj.getTransactionType()+obj.getTransactionAmount());
	        Transaction objAcc = new Transaction();
	        
	        BankAccount ob = dao.findByAccountNumber(AccountNumber);
	       
//	        objAcc.setTransactionAmount(obj.getTransactionAmount());
//          objAcc.setTransactionType(obj.getTransactionType());
//	        objAcc.setTransactionStatus("Success");
	       
	        
	        objAcc.setTransactionType(obj.getTransactionType());
	        double getbal=obj.getTransactionAmount();
	        double oldbal=ob.getBalance();
	        
	        if(obj.getTransactionType().toLowerCase().equals("deposit"))
	        {
	        	double newbal=getbal+oldbal;
	        	objAcc.setTransactionAmount(getbal);
	        	objAcc.setTransactionStatus("Success");
	        	ob.setBalance(newbal);
	        	
	        }
	        
	        if(obj.getTransactionType().toLowerCase().equals("withdraw"))
	        {
	        	
	        	if(oldbal>getbal)
	        	{
	        		double newbal=oldbal-getbal;
	        		if(ob.getAccountType().equals("Current"))
	        		{
	        			newbal=newbal-5;
	        		}
	        		objAcc.setTransactionAmount(getbal);
	        		objAcc.setTransactionStatus("Success");
	        		objAcc.setAccount(ob);
	        		ob.setBalance(newbal);
	        	}
	        	else
	        	{
	        		objAcc.setTransactionAmount(getbal);
	        		objAcc.setTransactionStatus("Failure");
	        		//objAcc.setAccount(ob);
	        	}
	        }
	       
	        objAcc.setAccount(ob);
	        ob.getTransaction().add(objAcc);
	      
	        da.save(objAcc);
	        dao.save(ob);
	        return objAcc;
	        //return da.findAll();
	          
	    }  
}